package com.michal.view.utilities;

import java.util.List;

public interface UserInput {
    int pickOption(List<Integer> list);

    String getNextString();
}
