package com.michal.view.utilities;

public interface Printer {
    void printMessage(String message);
}
