package com.michal.view.utilities;

public interface DaoChecker {
    boolean checkIfGradeBookIsEmpty();

    boolean checkIfAnyoneHasGrade1();

    boolean checkIfNoneGradesForAnyStudentToSubject();
}
