package com.michal.view.utilities.impl;

import com.michal.controller.PrivateSchoolController;
import com.michal.model.models.Student;
import com.michal.view.utilities.DaoChecker;
import com.michal.view.utilities.Printer;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class DaoCheckerImpl implements DaoChecker {
    private Printer printer;
    private PrivateSchoolController controller;

    @Override
    public boolean checkIfGradeBookIsEmpty() {
        if (controller.getStudentDAO().getListOfStudents().isEmpty()) {
            printer.printMessage("Grade book is empty, please add students first!");
            return true;
        }
        return false;
    }

    @Override
    public boolean checkIfAnyoneHasGrade1() {
        List<Student> listOfStudentWithGrade1 = controller.getListOfStudentsWithGrade1ThatWasNotReversed();
        if (!listOfStudentWithGrade1.isEmpty()) {
            printer.printMessage("Below students have have at least one grade 1, that has to be revised!");
            listOfStudentWithGrade1.forEach(student -> printer.printMessage(student.toString()));
            return true;
        }
        return false;
    }

    @Override
    public boolean checkIfNoneGradesForAnyStudentToSubject() {
        List<Student> noGradesStudentList = controller.getListOfStudentsWithNoGradesForSomeSubjects();
        if (!noGradesStudentList.isEmpty()) {
            printer.printMessage("Below students have no grades for at least one subject!");
            noGradesStudentList.forEach(student -> printer.printMessage(student.toString()));
            return true;
        }
        return false;
    }
}
