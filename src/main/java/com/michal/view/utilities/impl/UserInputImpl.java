package com.michal.view.utilities.impl;

import com.michal.view.utilities.CustomScanner;
import com.michal.view.utilities.Printer;
import com.michal.view.utilities.UserInput;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class UserInputImpl implements UserInput {
    private CustomScanner customScanner;
    private Printer printer;

    @Override
    public int pickOption(List<Integer> list) {
        int option = -1;
        boolean flag = true;
        while (flag) {
            option = customScanner.getNextInt();
            if (list.contains(option)) {
                flag = false;
            } else {
                printer.printMessage("Incorrect input, please try again.");
            }
        }
        return option;
    }

    @Override
    public String getNextString() {
        return customScanner.getNextString();
    }
}

