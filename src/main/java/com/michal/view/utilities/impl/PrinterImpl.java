package com.michal.view.utilities.impl;

import com.michal.view.utilities.Printer;

public class PrinterImpl implements Printer {
    @Override
    public void printMessage(String message) {
        System.out.println(message);
    }
}


