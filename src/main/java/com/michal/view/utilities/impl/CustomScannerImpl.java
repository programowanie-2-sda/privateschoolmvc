package com.michal.view.utilities.impl;

import com.michal.view.utilities.CustomScanner;

import java.util.InputMismatchException;
import java.util.Scanner;

public class CustomScannerImpl implements CustomScanner {
    private Scanner scanner = new Scanner(System.in);

    @Override
    public String getNextString() {
        return scanner.next();
    }

    @Override
    public int getNextInt() {
        int output = -1;
        boolean flag = true;
        while (flag) {
            try {
                output = scanner.nextInt();
                flag = false;
            } catch (InputMismatchException e) {
                System.out.println("Please provide number!");
                scanner.nextLine();
            }
        }
        return output;
    }
}
