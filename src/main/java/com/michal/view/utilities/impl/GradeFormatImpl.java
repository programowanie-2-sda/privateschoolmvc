package com.michal.view.utilities.impl;

import com.michal.view.utilities.GradeFormat;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class GradeFormatImpl implements GradeFormat {
    private final DecimalFormat df = new DecimalFormat("0.00");
    private final DecimalFormatSymbols dfs = new DecimalFormatSymbols();

    @Override
    public String format(double grade) {
        dfs.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(dfs);
        return df.format(grade);
    }
}
