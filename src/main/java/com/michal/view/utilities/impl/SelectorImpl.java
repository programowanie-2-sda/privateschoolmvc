package com.michal.view.utilities.impl;

import com.michal.controller.PrivateSchoolController;
import com.michal.view.utilities.Printer;
import com.michal.view.utilities.Selector;
import com.michal.view.utilities.UserInput;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class SelectorImpl implements Selector {
    private Printer printer;
    private PrivateSchoolController controller;
    private UserInput userInput;

    @Override
    public int selectGradeId() {
        printer.printMessage("Please enter grade value (1-6) or 0 for exit: ");
        return userInput.pickOption(List.of(0, 1, 2, 3, 4, 5, 6));
    }

    @Override
    public int selectSubjectId() {
        printer.printMessage("Please select subject:");
        controller.getSubjectDAO().getListOfSubject().forEach(subject -> printer.printMessage(subject.toString()));
        return userInput.pickOption(controller.getListOfSubjectId());
    }

    @Override
    public int selectStudentId() {
        printer.printMessage("Please select student's id: ");
        controller.getStudentDAO().getListOfStudents().forEach(student -> printer.printMessage(student.toString()));
        return userInput.pickOption(controller.getListOfStudentId());
    }
}
