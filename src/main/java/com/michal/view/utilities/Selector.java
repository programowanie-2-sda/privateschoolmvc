package com.michal.view.utilities;

public interface Selector {
    int selectGradeId();

    int selectSubjectId();

    int selectStudentId();
}
