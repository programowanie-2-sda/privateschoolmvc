package com.michal.view.utilities;

public interface CustomScanner {
    String getNextString();

    int getNextInt();
}
