package com.michal.view.utilities;

public interface GradeFormat {
    String format(double grade);
}
