package com.michal.view.menu;

import com.michal.view.utilities.Printer;

public class MenuItem extends MenuComponent {
    public MenuItem(String name, Printer printer) {
        super(name, printer);
    }

    @Override
    protected void print() {
        getPrinter().printMessage(getName());
    }
}
