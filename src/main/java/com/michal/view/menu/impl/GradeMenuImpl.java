package com.michal.view.menu.impl;

import com.michal.controller.PrivateSchoolController;
import com.michal.model.models.Student;
import com.michal.model.models.Subject;
import com.michal.model.models.SubjectName;
import com.michal.view.menu.MenuComposite;
import com.michal.view.menu.MenuItem;
import com.michal.view.utilities.*;
import com.michal.view.utilities.impl.GradeFormatImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class GradeMenuImpl extends MenuComposite {
    private final GradeFormat gradeFormat = new GradeFormatImpl();
    private PrivateSchoolController controller;
    private Selector selector;
    private DaoChecker daoChecker;

    GradeMenuImpl(UserInput userInput, PrivateSchoolController controller, Selector selector, DaoChecker daoChecker, Printer printer) {
        super("2. GRADE MENU", printer, userInput);
        this.controller = controller;
        this.daoChecker = daoChecker;
        this.selector = selector;
        List.of("0. Go back to main menu",
                "1. Add grades",
                "2. Revise grade",
                "3. Show one subject and all grades for student",
                "4. Show all subjects and all grades for student",
                "5. Show all subjects with averages for student",
                "6. End of school year").forEach(element -> add(new MenuItem(element, printer)));
    }

    @Override
    public void manage() {
        boolean flag = true;
        while (flag) {
            int option = getInputFromUser();
            switch (option) {
                case 0:
                    flag = false;
                    break;
                case 1:
                    addGrade();
                    break;
                case 2:
                    reviseGrade();
                    break;
                case 3:
                    showOneSubjectAndAllGradesForStudent();
                    break;
                case 4:
                    showAllSubjectsAndGradesForStudent();
                    break;
                case 5:
                    showAllSubjectsWithAveragesForStudent();
                    break;
                case 6:
                    if (!daoChecker.checkIfNoneGradesForAnyStudentToSubject() && !daoChecker.checkIfAnyoneHasGrade1()) {
                        finishYear();
                        flag = false;
                    }
                    break;
            }
        }
    }

    private void addGrade() {
        int studentId = selector.selectStudentId();
        int subjectId = selector.selectSubjectId();
        while (true) {
            int gradeId = selector.selectGradeId();
            if (gradeId == 0) {
                return;
            }
            controller.addGrade(studentId, subjectId, gradeId);
            getPrinter().printMessage("Grade added!");
        }
    }

    private void reviseGrade() {
        int studentId = selector.selectStudentId();
        Student student = controller.getStudentDAO().getStudent(studentId);
        int subjectId = selector.selectSubjectId() - 1;
        getPrinter().printMessage(student.toString() + ":");
        List<Integer> listOfGrades = showListsOfGrades(1, studentId, subjectId);
        if (listOfGrades.size() > 0) {
            int index = selectGradeIndex(listOfGrades) - 1;
            int newGrade = selector.selectGradeId();
            int flag = controller.reviseGrade(index, newGrade, studentId, subjectId + 1);
            if (flag == 0) {
                getPrinter().printMessage("Grade was revised in the past and cannot be revised again");
            } else if (flag == 1) {
                getPrinter().printMessage("Grade successfully revised");
            } else if (flag == 2) {
                getPrinter().printMessage("Student removed from a school, grade was revised to 1 - school's " +
                        "rules violation. ");
                controller.removeStudent(studentId);
            }
        }
    }

    private int selectGradeIndex(List<Integer> listOfGrades) {
        getPrinter().printMessage("Please select index of grade you would like to revise (indexing starts from 1):");
        List<Integer> listOfIndexes = Stream.iterate(1, x -> x + 1)
                .limit(listOfGrades.size() + 1)
                .collect(Collectors.toList());
        return getUserInput().pickOption(listOfIndexes);
    }

    private void showOneSubjectAndAllGradesForStudent() {
        int studentId = selector.selectStudentId();
        Student student = controller.getStudentDAO().getStudent(studentId);
        int subjectId = selector.selectSubjectId() - 1;
        getPrinter().printMessage(student.toString() + ":");
        showListsOfGrades(1, studentId, subjectId);
    }

    private void showAllSubjectsAndGradesForStudent() {
        int studentId = selector.selectStudentId();
        Student student = controller.getStudentDAO().getStudent(studentId);
        getPrinter().printMessage(student.toString() + ":");
        int subjectId = 0;
        showListsOfGrades(SubjectName.values().length, studentId, subjectId);
    }

    private void showAllSubjectsWithAveragesFor(int studentId) {
        int subjectId = 0;
        double average;
        for (int i = 0; i < SubjectName.values().length; i++) {
            subjectId += 1;
            Subject subject = controller.getSubjectDAO().getListOfSubject().get(subjectId - 1);
            average = controller.getGradesAverage(studentId, subjectId);
            displaySingleAverage(subject, average);
        }
    }

    private void showAllSubjectsWithAveragesForStudent() {
        int studentId = selector.selectStudentId();
        showAllSubjectsWithAveragesFor(studentId);
    }

    private void displaySingleAverage(Subject subject, double average) {
        String avgString = gradeFormat.format(average);
        getPrinter().printMessage(subject.getSubjectName() + " average: " + avgString);
    }

    private List<Integer> showListsOfGrades(int iteration, int studentId, int subjectId) {
        List<Integer> listOfGrades = new ArrayList<>();
        for (int i = 0; i < iteration; i++) {
            subjectId += 1;
            listOfGrades = controller.getListOfGradesForSingleSubject(studentId, subjectId);
            Subject subject = controller.getSubjectDAO().getListOfSubject().get(subjectId - 1);
            if (listOfGrades.size() == 0) {
                getPrinter().printMessage(subject.getSubjectName() + ": no grades");
            } else {
                showAllGradesForStudentAndSubject(subject, listOfGrades);
            }
        }
        return listOfGrades;
    }

    private void showAllGradesForStudentAndSubject(Subject subject, List<Integer> listOfGrades) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < listOfGrades.size() - 1; i++) {
            sb.append(listOfGrades.get(i));
            sb.append(", ");
        }
        sb.append(listOfGrades.get(listOfGrades.size() - 1));
        getPrinter().printMessage(subject.getSubjectName() + " grades: " + sb);
    }

    private void finishYear() {
        getPrinter().printMessage("----------------------------------------\n " +
                "YEAR END SUMMARY:" +
                "\n----------------------------------------");
        List<Integer> listOfStudentsId = controller.getListOfStudentId();
        for (int studentId : listOfStudentsId) {
            Student student = controller.getStudentDAO().getStudent(studentId);
            getPrinter().printMessage(student.toString() + ":");
            showAllSubjectsWithAveragesFor(studentId);
            double gpa = controller.getStudentGradePointAverage(studentId);
            String avgString = gradeFormat.format(gpa);
            getPrinter().printMessage("GRADE POINT AVERAGE: " + avgString + "\n");
            if (controller.isUnderMinimumGradePointAverage(gpa)) {
                getPrinter().printMessage("Student removed from a school\n ");
            }
        }
        controller.beginNewSchoolYear();
        getPrinter().printMessage("----------------------------------------\n " +
                "WELCOME TO NEW SCHOOL YEAR!!!!" +
                "\n----------------------------------------");
    }
}
