package com.michal.view.menu.impl;

import com.michal.controller.PrivateSchoolController;
import com.michal.model.models.Student;
import com.michal.view.menu.MenuComposite;
import com.michal.view.menu.MenuItem;
import com.michal.view.utilities.DaoChecker;
import com.michal.view.utilities.Printer;
import com.michal.view.utilities.Selector;
import com.michal.view.utilities.UserInput;

import java.util.List;

class StudentMenuImpl extends MenuComposite {
    private PrivateSchoolController controller;
    private Selector selector;
    private DaoChecker daoChecker;

    StudentMenuImpl(UserInput userInput, PrivateSchoolController controller, Selector selector, DaoChecker daoChecker, Printer printer) {
        super("1. STUDENT MENU", printer, userInput);
        this.controller = controller;
        this.daoChecker = daoChecker;
        this.selector = selector;
        List.of("0. Go back to main menu",
                "1. Add new student",
                "2. Remove student",
                "3. Show all students").forEach(element -> add(new MenuItem(element, printer)));
    }

    @Override
    public void manage() {
        boolean flag = true;
        while (flag) {
            int option = getInputFromUser();
            switch (option) {
                case 0: {
                    flag = false;
                }
                break;
                case 1: {
                    addStudent();
                }
                break;
                case 2: {
                    if (!daoChecker.checkIfGradeBookIsEmpty()) {
                        removeStudent();
                    }
                }
                break;
                case 3: {
                    if (!daoChecker.checkIfGradeBookIsEmpty()) {
                        controller.getStudentDAO().getListOfStudents()
                                .forEach(student -> getPrinter().printMessage(student.toString()));
                    }
                }
                break;
            }
        }
    }

    private void addStudent() {
        getPrinter().printMessage("Please provide student name: ");
        String name = getUserInput().getNextString();
        Student student = controller.addStudent(name);
        getPrinter().printMessage("New student added: " + student);
    }

    private void removeStudent() {
        int studentId = selector.selectStudentId();
        controller.removeStudent(studentId);
        getPrinter().printMessage("Student removed from a school");
    }
}
