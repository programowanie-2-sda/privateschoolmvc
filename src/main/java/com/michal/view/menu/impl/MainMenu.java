package com.michal.view.menu.impl;

import com.michal.controller.PrivateSchoolController;
import com.michal.view.menu.MenuComposite;
import com.michal.view.menu.MenuItem;
import com.michal.view.utilities.DaoChecker;
import com.michal.view.utilities.Printer;
import com.michal.view.utilities.Selector;
import com.michal.view.utilities.UserInput;

import java.util.List;

public class MainMenu extends MenuComposite {
    private DaoChecker daoChecker;

    public MainMenu(UserInput userInput, PrivateSchoolController controller, Selector selector, DaoChecker daoChecker, Printer printer) {
        super("0. MAIN MENU", printer, userInput);
        this.daoChecker = daoChecker;
        List.of(new MenuItem("0. Exit", printer),
                new StudentMenuImpl(userInput, controller, selector, daoChecker, printer),
                new GradeMenuImpl(userInput, controller, selector, daoChecker, printer)).forEach(this::add);
    }

    @Override
    public void manage() {
        boolean flag = true;
        while (flag) {
            int option = getInputFromUser();
            switch (option) {
                case 0:
                    getPrinter().printMessage("Good bye!");
                    flag = false;
                    break;
                case 1:
                    getMenuComponents().get(1).manage();
                    break;
                case 2:
                    if (!daoChecker.checkIfGradeBookIsEmpty()) {
                        getMenuComponents().get(2).manage();
                    }
                    break;
            }
        }
    }
}

