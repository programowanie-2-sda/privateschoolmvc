package com.michal.view.menu;

import com.michal.view.utilities.Printer;
import lombok.AccessLevel;
import lombok.Getter;

@Getter(AccessLevel.PROTECTED)
public abstract class MenuComponent {
    private String name;
    private Printer printer;

    MenuComponent(String name, Printer printer) {
        this.name = name;
        this.printer = printer;
    }

    public void manage() {
        throw new UnsupportedOperationException();
    }

    protected abstract void print();
}
