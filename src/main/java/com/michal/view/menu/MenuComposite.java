package com.michal.view.menu;

import com.michal.view.utilities.Printer;
import com.michal.view.utilities.UserInput;
import lombok.AccessLevel;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter(AccessLevel.PROTECTED)
public abstract class MenuComposite extends MenuComponent {
    private UserInput userInput;
    private ArrayList<MenuComponent> menuComponents = new ArrayList<>();

    protected MenuComposite(String name, Printer printer, UserInput userInput) {
        super(name, printer);
        this.userInput = userInput;
    }

    @Override
    protected void print() {
        getPrinter().printMessage("-------------------------------------");
        getPrinter().printMessage(getName().substring(getName().indexOf('.') + 1));
        getPrinter().printMessage("-------------------------------------");
        menuComponents.forEach(menuElement -> getPrinter().printMessage(menuElement.getName()));
    }

    protected int getInputFromUser() {
        print();
        return userInput.pickOption(createListOfIntegersFromZeroTo(menuComponents.size()));
    }

    protected void add(MenuComponent menuComponent) {
        menuComponents.add(menuComponent);
    }

    private List<Integer> createListOfIntegersFromZeroTo(int threshold) {
        return Stream.iterate(0, x -> x + 1)
                .limit(threshold)
                .collect(Collectors.toList());
    }
}
