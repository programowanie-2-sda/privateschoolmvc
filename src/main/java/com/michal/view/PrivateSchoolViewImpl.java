package com.michal.view;

import com.michal.controller.PrivateSchoolController;
import com.michal.controller.impl.PrivateSchoolControllerImpl;
import com.michal.view.menu.MenuComponent;
import com.michal.view.menu.impl.MainMenu;
import com.michal.view.utilities.*;
import com.michal.view.utilities.impl.*;

public class PrivateSchoolViewImpl implements PrivateSchoolView {
    private Printer printer = new PrinterImpl();
    private CustomScanner customScanner = new CustomScannerImpl();
    private UserInput userInput = new UserInputImpl(customScanner, printer);
    private PrivateSchoolController controller = new PrivateSchoolControllerImpl();
    private DaoChecker daoChecker = new DaoCheckerImpl(printer, controller);
    private Selector selector = new SelectorImpl(printer, controller, userInput);
    private MenuComponent mainMenu = new MainMenu(userInput, controller, selector, daoChecker, printer);

    @Override
    public void runApp() {
        mainMenu.manage();
    }
}


