package com.michal.model.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class StudentToSubject {
    private int id;
    private int studentId;
    private int subjectId;
}
