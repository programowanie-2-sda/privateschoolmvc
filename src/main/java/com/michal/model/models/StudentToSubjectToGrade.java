package com.michal.model.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentToSubjectToGrade {
    private int id;
    private int studentToSubjectId;
    private int gradeId;
    private boolean reversalFlag;

    public StudentToSubjectToGrade(int id, int studentToSubjectId, int gradeId) {
        this.id = id;
        this.studentToSubjectId = studentToSubjectId;
        this.gradeId = gradeId;
    }
}
