package com.michal.model.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
@Getter
public class Subject {
    private int id;
    private SubjectName subjectName;
}
