package com.michal.model.models;

public enum SubjectName {
    MATHEMATICS,
    PHYSICS,
    HISTORY
}
