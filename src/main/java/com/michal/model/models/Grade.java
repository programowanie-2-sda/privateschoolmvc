package com.michal.model.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Grade {
    private int id;
    private int value;
}
