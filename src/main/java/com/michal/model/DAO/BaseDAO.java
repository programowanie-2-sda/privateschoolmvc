package com.michal.model.DAO;

public interface BaseDAO<T> {
    void add(T object);

    void remove(T object);
}
