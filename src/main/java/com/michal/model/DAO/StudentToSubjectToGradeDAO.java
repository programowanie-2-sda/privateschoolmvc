package com.michal.model.DAO;

import com.michal.model.models.StudentToSubjectToGrade;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class StudentToSubjectToGradeDAO implements BaseDAO<StudentToSubjectToGrade> {
    private List<StudentToSubjectToGrade> listSSG = new ArrayList<>();

    @Override
    public void add(StudentToSubjectToGrade ssg) {
        listSSG.add(ssg);
    }

    @Override
    public void remove(StudentToSubjectToGrade ssg) {
        listSSG.remove(ssg);
    }

    public List<StudentToSubjectToGrade> getListOfStudentToSubjectToGradeFor(int studentToSubjectId) {
        return listSSG.stream()
                .filter(studentToSubjectToGrade -> studentToSubjectToGrade.getStudentToSubjectId() == studentToSubjectId)
                .collect(Collectors.toList());
    }
}
