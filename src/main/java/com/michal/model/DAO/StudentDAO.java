package com.michal.model.DAO;

import com.michal.model.models.Student;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class StudentDAO implements BaseDAO<Student> {
    private List<Student> listOfStudents = new ArrayList<>();

    public Student getStudent(int studentId) {
        return listOfStudents.stream()
                .filter(student -> student.getId() == studentId)
                .findFirst()
                .orElse(null);
    }

    @Override
    public void add(Student student) {
        listOfStudents.add(student);
    }

    @Override
    public void remove(Student student) {
        listOfStudents.remove(student);
    }

    public List<Integer> getListOfIds() {
        return listOfStudents.stream()
                .map(Student::getId)
                .collect(Collectors.toList());
    }


}
