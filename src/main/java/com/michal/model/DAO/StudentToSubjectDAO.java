package com.michal.model.DAO;

import com.michal.model.models.StudentToSubject;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class StudentToSubjectDAO implements BaseDAO<StudentToSubject> {
    private List<StudentToSubject> listSS = new ArrayList<>();

    @Override
    public void add(StudentToSubject ss) {
        listSS.add(ss);
    }

    @Override
    public void remove(StudentToSubject ss) {
        listSS.remove(ss);
    }

    public List<StudentToSubject> getListOfStudentToSubjectFor(int studentId) {
        return listSS.stream()
                .filter(studentToSubject -> studentToSubject.getStudentId() == studentId)
                .collect(Collectors.toList());
    }
}
