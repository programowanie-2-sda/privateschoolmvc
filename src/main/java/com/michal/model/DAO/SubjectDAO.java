package com.michal.model.DAO;

import com.michal.model.models.Subject;
import com.michal.model.models.SubjectName;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class SubjectDAO implements BaseDAO<Subject> {
    private List<Subject> listOfSubject = new ArrayList<>();

    public SubjectDAO() {
        Arrays.stream(SubjectName.values()).
                forEach(subjectName -> listOfSubject.add(new Subject(subjectName.ordinal() + 1, subjectName)));
    }

    @Override
    public void add(Subject subject) {
        listOfSubject.add(subject);
    }

    @Override
    public void remove(Subject subject) {
        listOfSubject.remove(subject);
    }

    public List<Integer> getListOfIds() {
        return listOfSubject.stream()
                .map(Subject::getId)
                .collect(Collectors.toList());
    }
}
