package com.michal.model.DAO;

import com.michal.model.models.Grade;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class GradeDAO {
    private List<Grade> listOfGrades = new ArrayList<>();

    public GradeDAO() {
        for (int i = 1; i < 7; i++) {
            listOfGrades.add(new Grade(i, i));
        }
    }
}
