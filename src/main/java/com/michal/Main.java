package com.michal;

import com.michal.view.PrivateSchoolView;
import com.michal.view.PrivateSchoolViewImpl;

public class Main {
    public static void main(String[] args) {
        PrivateSchoolView view = new PrivateSchoolViewImpl();
        view.runApp();
    }
}
