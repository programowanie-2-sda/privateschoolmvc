package com.michal.controller.impl;

import com.michal.controller.DaoElementsFinder;
import com.michal.controller.GradeController;
import com.michal.model.DAO.GradeDAO;
import com.michal.model.DAO.StudentDAO;
import com.michal.model.DAO.StudentToSubjectToGradeDAO;
import com.michal.model.models.Student;
import com.michal.model.models.StudentToSubjectToGrade;
import com.michal.model.models.SubjectName;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
class GradeControllerImpl implements GradeController {
    private final double minGradePointAverage = 4.3;
    private StudentDAO studentDAO;
    private GradeDAO gradeDAO;
    private StudentToSubjectToGradeDAO studentToSubjectToGradeDAO;
    private DaoElementsFinder daoElementsFinder;

    @Override
    public double getMinGradePointAverage() {
        return minGradePointAverage;
    }

    @Override
    public void addGrade(int studentId, int subjectId, int gradeId) {
        int studentToSubjectId = daoElementsFinder.findIdOfStudentToSubjectFor(studentId, subjectId);
        StudentToSubjectToGrade ssg = createStudentToSubjectToGrade(studentToSubjectId, gradeId);
        studentToSubjectToGradeDAO.add(ssg);
    }

    @Override
    public double getGradesAverage(int studentId, int subjectId) {
        int studentToSubjectId = daoElementsFinder.findIdOfStudentToSubjectFor(studentId, subjectId);
        return studentToSubjectToGradeDAO.getListSSG().stream()
                .filter(studentToSubjectToGrade -> studentToSubjectToGrade.getStudentToSubjectId() == studentToSubjectId)
                .mapToInt(StudentToSubjectToGrade::getGradeId)
                .map(gradeId -> gradeDAO.getListOfGrades().get(gradeId - 1).getValue())
                .average()
                .orElse(0);
    }

    @Override
    public double getStudentGradePointAverage(int studentId) {
        List<Double> listOfAverages = new ArrayList<>();
        Arrays.stream(SubjectName.values())
                .forEach(element -> listOfAverages.add(getGradesAverage(studentId, element.ordinal() + 1)));
        return listOfAverages.stream()
                .mapToDouble(Double::doubleValue)
                .average()
                .orElse(0.0);
    }

    @Override
    public int reviseGrade(int indexOfGrade, int newGrade, int studentId, int subjectId) {
        List<Integer> listOfSSGIndexes =
                getListOfStudentToSubjectToGradeIds(studentId, subjectId);
        int idOfSSGForGradeToBeChanged = listOfSSGIndexes.get(indexOfGrade);
        boolean flag = studentToSubjectToGradeDAO.getListSSG().get(idOfSSGForGradeToBeChanged - 1).isReversalFlag();
        if (flag) {
            return 0;
        } else {
            if (newGrade == 1) {
                return 2;
            } else {
                studentToSubjectToGradeDAO.getListSSG().get(idOfSSGForGradeToBeChanged - 1).setReversalFlag(true);
                studentToSubjectToGradeDAO.getListSSG().get(idOfSSGForGradeToBeChanged - 1).setGradeId(newGrade);
                return 1;
            }
        }
    }

    @Override
    public void clearStudentToSubjectToGradeDao() {
        studentToSubjectToGradeDAO.setListSSG(new ArrayList<>());
    }

    @Override
    public List<Integer> getListOfStudentsIdUnderMinimumGradePointAverage() {
        return studentDAO.getListOfStudents().stream()
                .collect(Collectors.toMap(Student::getId, student -> getStudentGradePointAverage(student.getId())))
                .entrySet().stream()
                .filter(entry -> entry.getValue() < minGradePointAverage)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }


    private StudentToSubjectToGrade createStudentToSubjectToGrade(int StudentToSubjectId, int gradeId) {
        int id;
        if (studentToSubjectToGradeDAO.getListSSG().isEmpty()) {
            id = 1;
        } else {
            id = studentToSubjectToGradeDAO.getListSSG().get(studentToSubjectToGradeDAO.getListSSG()
                    .size() - 1).getId() + 1;
        }
        return new StudentToSubjectToGrade(id, StudentToSubjectId, gradeId);
    }

    private List<Integer> getListOfStudentToSubjectToGradeIds(int studentId, int subjectId) {
        int studentToSubjectId = daoElementsFinder.findIdOfStudentToSubjectFor(studentId, subjectId);
        return studentToSubjectToGradeDAO.getListSSG().stream()
                .filter(studentToSubjectToGrade -> studentToSubjectToGrade.getStudentToSubjectId()
                        == studentToSubjectId)
                .map(StudentToSubjectToGrade::getId)
                .collect(Collectors.toList());
    }
}

