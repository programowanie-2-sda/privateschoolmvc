package com.michal.controller.impl;

import com.michal.controller.StudentController;
import com.michal.model.DAO.StudentDAO;
import com.michal.model.DAO.StudentToSubjectDAO;
import com.michal.model.DAO.StudentToSubjectToGradeDAO;
import com.michal.model.models.Student;
import com.michal.model.models.StudentToSubject;
import com.michal.model.models.SubjectName;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.List;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
class StudentControllerImpl implements StudentController {
    private StudentDAO studentDAO;
    private StudentToSubjectDAO studentToSubjectDAO;
    private StudentToSubjectToGradeDAO studentToSubjectToGradeDAO;

    @Override
    public Student addStudent(String name) {
        Student student = createStudent(name);
        studentDAO.add(student);
        Arrays.stream(SubjectName.values())
                .forEach(subjectName ->
                        studentToSubjectDAO.add(createStudentToSubject(student, subjectName.ordinal() + 1)));
        return student;
    }

    @Override
    public void removeStudent(int studentId) {
        Student student = studentDAO.getStudent(studentId);
        studentDAO.remove(student);
        List<StudentToSubject> listSS = studentToSubjectDAO.getListOfStudentToSubjectFor(studentId);
        listSS.forEach(studentToSubject -> {
            studentToSubjectDAO.remove(studentToSubject);
            studentToSubjectToGradeDAO.getListOfStudentToSubjectToGradeFor(studentId)
                    .forEach(studentToSubjectToGrade -> studentToSubjectToGradeDAO.remove(studentToSubjectToGrade));
        });
    }

    private Student createStudent(String name) {
        int id;
        if (studentDAO.getListOfStudents().isEmpty()) {
            id = 1;
        } else {
            id = studentDAO.getListOfStudents().get(studentDAO.getListOfStudents().size() - 1).getId() + 1;
        }
        return new Student(id, name);
    }

    private StudentToSubject createStudentToSubject(Student student, int subjectId) {
        int id;
        if (studentToSubjectDAO.getListSS().isEmpty()) {
            id = 1;
        } else {
            id = studentToSubjectDAO.getListSS().get(studentToSubjectDAO.getListSS().size() - 1).getId() + 1;
        }
        return new StudentToSubject(id, student.getId(), subjectId);
    }
}

