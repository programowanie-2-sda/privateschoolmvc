package com.michal.controller.impl;

import com.michal.controller.DaoElementsFinder;
import com.michal.model.DAO.GradeDAO;
import com.michal.model.DAO.StudentDAO;
import com.michal.model.DAO.StudentToSubjectDAO;
import com.michal.model.DAO.StudentToSubjectToGradeDAO;
import com.michal.model.models.*;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
class DaoElementsFinderImpl implements DaoElementsFinder {
    private StudentDAO studentDAO;
    private StudentToSubjectDAO studentToSubjectDAO;
    private StudentToSubjectToGradeDAO studentToSubjectToGradeDAO;
    private GradeDAO gradeDao;

    @Override
    public List<Student> getListOfStudentsWithGrade1ThatWasNotReversed() {
        List<Integer> listOfStudentToSubjectId = studentToSubjectToGradeDAO.getListSSG().stream()
                .filter(studentToSubjectToGrade -> studentToSubjectToGrade.getGradeId() == 1
                        && !studentToSubjectToGrade.isReversalFlag())
                .map(StudentToSubjectToGrade::getStudentToSubjectId)
                .collect(Collectors.toList());
        return transformListOfStudentToSubjectIdsToListOfStudents(listOfStudentToSubjectId);
    }

    @Override
    public List<Student> getListOfStudentsWithNoGradesForSomeSubjects() {
        List<Integer> listOfStudentToSubjectId = getMapOfStudentToSubjectIdToListOfGrades().entrySet().stream()
                .filter(entry -> entry.getValue().isEmpty())
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        return transformListOfStudentToSubjectIdsToListOfStudents(listOfStudentToSubjectId);
    }

    @Override
    public List<Integer> getListOfGradesValuesFor(int studentId, int subjectId) {
        int studentToSubjectId = findIdOfStudentToSubjectFor(studentId, subjectId);
        return studentToSubjectToGradeDAO.getListSSG().stream()
                .filter(studentToSubjectToGrade -> studentToSubjectToGrade.getStudentToSubjectId() == studentToSubjectId)
                .map(StudentToSubjectToGrade::getGradeId)
                .map(gradeId -> gradeDao.getListOfGrades().stream()
                        .filter(grade -> grade.getId() == gradeId)
                        .map(Grade::getValue)
                        .findFirst()
                        .orElse(0))
                .collect(Collectors.toList());
    }

    @Override
    public int findIdOfStudentToSubjectFor(int studentId, int subjectId) {
        return studentToSubjectDAO.getListSS().stream()
                .filter(studentToSubject -> studentToSubject.getStudentId() == studentId &&
                        studentToSubject.getSubjectId() == subjectId)
                .mapToInt(StudentToSubject::getId)
                .findFirst()
                .orElse(0);
    }

    private List<Student> transformListOfStudentToSubjectIdsToListOfStudents(List<Integer> listOfStudentToSubjectId) {
        return listOfStudentToSubjectId.stream()
                .map(studentToSubjectId -> studentToSubjectDAO.getListSS().stream()
                        .filter(studentToSubject -> studentToSubject.getId() == studentToSubjectId)
                        .findFirst()
                        .orElse(null))
                .map(StudentToSubject::getStudentId)
                .map(studentId -> studentDAO.getListOfStudents().stream()
                        .filter(student -> student.getId() == studentId)
                        .findFirst()
                        .orElse(null))
                .distinct()
                .collect(Collectors.toList());
    }

    private Map<Integer, List<Integer>> getMapOfStudentToSubjectIdToListOfGrades() {
        List<Integer> listOfStudentId = studentDAO.getListOfIds();
        Map<Integer, List<Integer>> map = new HashMap<>();
        listOfStudentId.forEach(integer -> {
            List<Integer> listOfGrades;
            for (int j = 1; j < SubjectName.values().length + 1; j++) {
                int studentToSubjectId = findIdOfStudentToSubjectFor(integer, j);
                listOfGrades = getListOfGradesValuesFor(integer, j);
                map.put(studentToSubjectId, listOfGrades);
            }
        });
        return map;
    }
}

