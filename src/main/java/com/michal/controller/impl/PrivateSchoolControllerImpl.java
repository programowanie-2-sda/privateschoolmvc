package com.michal.controller.impl;

import com.michal.controller.DaoElementsFinder;
import com.michal.controller.GradeController;
import com.michal.controller.PrivateSchoolController;
import com.michal.controller.StudentController;
import com.michal.model.DAO.*;
import com.michal.model.models.Student;

import java.util.List;

public class PrivateSchoolControllerImpl implements PrivateSchoolController {
    private StudentDAO studentDAO = new StudentDAO();
    private SubjectDAO subjectDAO = new SubjectDAO();
    private GradeDAO gradeDAO = new GradeDAO();
    private StudentToSubjectDAO studentToSubjectDAO = new StudentToSubjectDAO();
    private StudentToSubjectToGradeDAO studentToSubjectToGradeDAO = new StudentToSubjectToGradeDAO();
    private DaoElementsFinder daoElementsFinder = new DaoElementsFinderImpl(studentDAO, studentToSubjectDAO, studentToSubjectToGradeDAO, gradeDAO);
    private StudentController studentController = new StudentControllerImpl(studentDAO, studentToSubjectDAO, studentToSubjectToGradeDAO);
    private GradeController gradeController = new GradeControllerImpl(studentDAO, gradeDAO, studentToSubjectToGradeDAO, daoElementsFinder);

    @Override
    public StudentDAO getStudentDAO() {
        return studentDAO;
    }

    @Override
    public SubjectDAO getSubjectDAO() {
        return subjectDAO;
    }

    @Override
    public StudentToSubjectToGradeDAO getStudentToSubjectToGradeDAO() {
        return studentToSubjectToGradeDAO;
    }

    @Override
    public List<Integer> getListOfGradesForSingleSubject(int studentId, int subjectId) {
        return daoElementsFinder.getListOfGradesValuesFor(studentId, subjectId);
    }

    @Override
    public Student addStudent(String name) {
        return studentController.addStudent(name);
    }

    @Override
    public void addGrade(int studentId, int subjectId, int gradeId) {
        gradeController.addGrade(studentId, subjectId, gradeId);
    }

    @Override
    public List<Integer> getListOfStudentId() {
        return studentDAO.getListOfIds();
    }

    @Override
    public List<Integer> getListOfSubjectId() {
        return subjectDAO.getListOfIds();
    }

    @Override
    public double getGradesAverage(int studentId, int subjectId) {
        return gradeController.getGradesAverage(studentId, subjectId);
    }

    @Override
    public void removeStudent(int studentId) {
        studentController.removeStudent(studentId);
    }

    @Override
    public int reviseGrade(int indexOfGrade, int newGrade, int studentId, int subjectId) {
        return gradeController.reviseGrade(indexOfGrade, newGrade, studentId, subjectId);
    }

    @Override
    public List<Student> getListOfStudentsWithNoGradesForSomeSubjects() {
        return daoElementsFinder.getListOfStudentsWithNoGradesForSomeSubjects();
    }

    @Override
    public List<Student> getListOfStudentsWithGrade1ThatWasNotReversed() {
        return daoElementsFinder.getListOfStudentsWithGrade1ThatWasNotReversed();
    }

    @Override
    public double getStudentGradePointAverage(int studentId) {
        return gradeController.getStudentGradePointAverage(studentId);
    }

    @Override
    public Student getStudent(int studentId) {
        return studentDAO.getStudent(studentId);
    }

    @Override
    public void beginNewSchoolYear() {
        gradeController.getListOfStudentsIdUnderMinimumGradePointAverage()
                .forEach(studentId -> studentController.removeStudent(studentId));
        gradeController.clearStudentToSubjectToGradeDao();
    }

    @Override
    public List<Student> getListOfStudents() {
        return studentDAO.getListOfStudents();
    }

    @Override
    public boolean isUnderMinimumGradePointAverage(double average) {
        return average < gradeController.getMinGradePointAverage();
    }
}
