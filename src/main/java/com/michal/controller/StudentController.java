package com.michal.controller;

import com.michal.model.models.Student;

public interface StudentController {
    Student addStudent(String name);

    void removeStudent(int studentId);
}
