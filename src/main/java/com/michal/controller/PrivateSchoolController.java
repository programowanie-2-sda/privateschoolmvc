package com.michal.controller;

import com.michal.model.DAO.StudentDAO;
import com.michal.model.DAO.StudentToSubjectToGradeDAO;
import com.michal.model.DAO.SubjectDAO;
import com.michal.model.models.Student;

import java.util.List;

public interface PrivateSchoolController {
    Student addStudent(String name);

    void addGrade(int studentId, int subjectId, int gradeId);

    List<Integer> getListOfStudentId();

    List<Integer> getListOfSubjectId();

    StudentDAO getStudentDAO();

    SubjectDAO getSubjectDAO();

    StudentToSubjectToGradeDAO getStudentToSubjectToGradeDAO();

    List<Integer> getListOfGradesForSingleSubject(int studentId, int subjectId);

    double getGradesAverage(int studentId, int subjectId);

    int reviseGrade(int indexOfGrade, int newGrade, int studentId, int subjectId);

    List<Student> getListOfStudentsWithNoGradesForSomeSubjects();

    List<Student> getListOfStudentsWithGrade1ThatWasNotReversed();

    List<Student> getListOfStudents();

    double getStudentGradePointAverage(int studentId);

    void removeStudent(int studentId);

    Student getStudent(int studentId);

    boolean isUnderMinimumGradePointAverage(double average);

    void beginNewSchoolYear();
}

