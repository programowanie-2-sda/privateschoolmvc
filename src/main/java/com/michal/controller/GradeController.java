package com.michal.controller;

import java.util.List;

public interface GradeController {
    void addGrade(int studentId, int subjectId, int gradeId);

    double getGradesAverage(int studentId, int subjectId);

    double getStudentGradePointAverage(int studentId);

    int reviseGrade(int indexOfGrade, int newGrade, int studentId, int subjectId);

    void clearStudentToSubjectToGradeDao();

    List<Integer> getListOfStudentsIdUnderMinimumGradePointAverage();

    double getMinGradePointAverage();
}
