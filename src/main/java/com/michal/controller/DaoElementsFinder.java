package com.michal.controller;

import com.michal.model.models.Student;

import java.util.List;

public interface DaoElementsFinder {
    List<Student> getListOfStudentsWithGrade1ThatWasNotReversed();

    List<Student> getListOfStudentsWithNoGradesForSomeSubjects();

    List<Integer> getListOfGradesValuesFor(int studentId, int subjectId);

    int findIdOfStudentToSubjectFor(int studentId, int subjectId);
}
