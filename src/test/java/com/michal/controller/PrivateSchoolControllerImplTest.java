package com.michal.controller;

import com.michal.controller.impl.PrivateSchoolControllerImpl;
import com.michal.model.models.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class PrivateSchoolControllerImplTest {
    private PrivateSchoolControllerImpl tested = new PrivateSchoolControllerImpl();
    private Student student1;

    @BeforeEach
    private void setUp() {
        Student student1 = tested.addStudent("Student1");
    }

    @Test
    @DisplayName("should return 1 when user wants to revise grade for the first time")
    public void should_revise_grade() {
        tested.addGrade(1, 1, 1);
        Assertions.assertEquals(1, tested.reviseGrade(0, 6, 1, 1));
    }

    @Test
    @DisplayName("should return 0 when user revised grade to the same one and then try to revise again " +
            "to the same one")
    public void should_not_revise_grade_2() {
        tested.addGrade(1, 1, 2);
        tested.reviseGrade(0, 2, 1, 1);
        Assertions.assertEquals(0, tested.reviseGrade(0, 2, 1, 1));
    }

    @Test
    @DisplayName("should return 0 when user revised grade to the same one and then try to revise again " +
            "to new one")
    public void should_not_revise_grade_3() {
        tested.addGrade(1, 1, 2);
        tested.reviseGrade(0, 2, 1, 1);
        Assertions.assertEquals(0, tested.reviseGrade(0, 5, 1, 1));
    }

    @Test
    @DisplayName("should return 2 when user revised grade to 1")
    public void should_return_2_when_revised_grade_is_1() {
        tested.addGrade(1, 1, 2);
        Assertions.assertEquals(2, tested.reviseGrade(0, 1, 1, 1));
    }

    @Test
    public void should_return_student_list_size_2_when_removing_student() {
        tested.addStudent("Student2");
        tested.addStudent("Student3");
        tested.removeStudent(2);
        Assertions.assertEquals(2, tested.getListOfStudents().size());
    }

    @Test
    public void should_return_student_based_on_his_id() {
        Assertions.assertEquals("Student1", tested.getStudent(1).getName());
    }

    @Test
    public void should_return_student_GPA_when_student_has_at_least_one_grade_for_each_subject() {
        tested.addGrade(1, 1, 1);
        tested.addGrade(1, 2, 2);
        tested.addGrade(1, 3, 3);
        Assertions.assertEquals(2, tested.getStudentGradePointAverage(1));
    }

    @Test
    public void should_return_student_GPA_as_0_when_student_has_no_grades_at_all() {
        Assertions.assertEquals(0, tested.getStudentGradePointAverage(1));
    }

    @Test
    @DisplayName("should return list of 2 students - they have no grades at all")
    public void test_getListOfStudentsWithNoGradesForSomeSubjects_1() {
        tested.addStudent("Student2");
        Assertions.assertEquals(2, tested.getListOfStudentsWithNoGradesForSomeSubjects().size());
    }

    @Test
    @DisplayName("should return list of 2 students - they have no grades for 1 or 2 subject")
    public void test_getListOfStudentsWithNoGradesForSomeSubjects_2() {
        tested.addStudent("Student2");
        tested.addGrade(1, 1, 1);
        Assertions.assertEquals(2, tested.getListOfStudentsWithNoGradesForSomeSubjects().size());
    }

    @Test
    @DisplayName("should return empty list of students - they have at least one grade for all subjects")
    public void test_getListOfStudentsWithNoGradesForSomeSubjects_3() {
        tested.addStudent("Student2");
        tested.addGrade(1, 1, 1);
        tested.addGrade(1, 2, 1);
        tested.addGrade(1, 3, 1);
        tested.addGrade(2, 1, 1);
        tested.addGrade(2, 2, 1);
        tested.addGrade(2, 3, 1);
        Assertions.assertEquals(0, tested.getListOfStudentsWithNoGradesForSomeSubjects().size());
    }

    @Test
    @DisplayName("should return list of 2 students - they have grades 1 for all subjects")
    public void test_getListOfStudentsWithGrade1_1() {
        tested.addStudent("Student2");
        tested.addGrade(1, 1, 1);
        tested.addGrade(1, 2, 1);
        tested.addGrade(1, 3, 1);
        tested.addGrade(2, 1, 1);
        tested.addGrade(2, 2, 1);
        tested.addGrade(2, 3, 1);
        Assertions.assertEquals(2, tested.getListOfStudentsWithGrade1ThatWasNotReversed().size());
    }

    @Test
    @DisplayName("should return list of 2 students - they have grades 1 for 1 or 2 subjects and no grade for 3rd one")
    public void test_getListOfStudentsWithGrade1_2() {
        tested.addStudent("Student2");
        tested.addGrade(1, 1, 6);
        tested.addGrade(1, 3, 1);
        tested.addGrade(2, 1, 1);
        tested.addGrade(2, 2, 1);
        Assertions.assertEquals(2, tested.getListOfStudentsWithGrade1ThatWasNotReversed().size());
    }

    @Test
    @DisplayName("should return empty list of students - they do not have grade 1 at all")
    public void test_getListOfStudentsWithGrade1_3() {
        tested.addStudent("Student2");
        tested.addGrade(1, 1, 6);
        tested.addGrade(1, 3, 4);
        tested.addGrade(2, 1, 5);
        tested.addGrade(2, 2, 2);
        Assertions.assertEquals(0, tested.getListOfStudentsWithGrade1ThatWasNotReversed().size());
    }

    @Test
    public void should_delete_all_grades() {
        tested.addStudent("Student2");
        tested.addGrade(1, 1, 6);
        tested.addGrade(1, 3, 4);
        tested.addGrade(2, 1, 5);
        tested.addGrade(2, 2, 2);
        tested.beginNewSchoolYear();
        Assertions.assertEquals(0, tested.getStudentToSubjectToGradeDAO().getListSSG().size());
    }
}