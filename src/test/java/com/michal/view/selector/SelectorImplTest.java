package com.michal.view.selector;

import com.michal.controller.impl.PrivateSchoolControllerImpl;
import com.michal.view.utilities.Selector;
import com.michal.view.utilities.impl.SelectorImpl;
import com.michal.view.utilities.Printer;
import com.michal.view.utilities.UserInput;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

class SelectorImplTest {
    private Printer printerMock = Mockito.mock(Printer.class);
    private PrivateSchoolControllerImpl controller = new PrivateSchoolControllerImpl();
    private UserInput userInputMock = Mockito.mock(UserInput.class);
    private Selector tested = new SelectorImpl(printerMock, controller, userInputMock);

    @Test
    void should_return_grade_id_2() {
        when(userInputMock.pickOption(Mockito.anyList())).thenReturn(2);
        Assertions.assertEquals(2, tested.selectGradeId());
    }

    @Test
    void should_return_subject_id_2() {
        when(userInputMock.pickOption(Mockito.anyList())).thenReturn(2);
        Assertions.assertEquals(2, tested.selectSubjectId());
    }

    @Test
    void should_return_student_id_2() {
        when(userInputMock.pickOption(Mockito.anyList())).thenReturn(2);
        Assertions.assertEquals(2, tested.selectStudentId());
    }
}