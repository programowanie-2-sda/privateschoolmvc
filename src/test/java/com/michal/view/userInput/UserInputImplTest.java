package com.michal.view.userInput;

import com.michal.view.utilities.CustomScanner;
import com.michal.view.utilities.Printer;
import com.michal.view.utilities.impl.UserInputImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.mockito.Mockito.when;

class UserInputImplTest {
    private Printer printerMock = Mockito.mock(Printer.class);
    private CustomScanner customScannerMock = Mockito.mock(CustomScanner.class);
    private UserInputImpl tested = new UserInputImpl(customScannerMock, printerMock);

    @Test
    public void should_return_option_2() {
        when(customScannerMock.getNextInt()).thenReturn(2);
        Assertions.assertEquals(2, tested.pickOption(List.of(0, 1, 2)));
    }

    @Test
    public void should_print_incorrect_input_msg() {
        when(customScannerMock.getNextInt()).thenReturn(2).thenReturn(1);
        tested.pickOption(List.of(0, 1));
        Mockito.verify(printerMock).printMessage("Incorrect input, please try again.");
    }
}