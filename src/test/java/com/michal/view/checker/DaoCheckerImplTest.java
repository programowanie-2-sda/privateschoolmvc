package com.michal.view.checker;

import com.michal.controller.impl.PrivateSchoolControllerImpl;
import com.michal.view.utilities.impl.DaoCheckerImpl;
import com.michal.view.utilities.Printer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class DaoCheckerImplTest {
    private Printer printerMock = Mockito.mock(Printer.class);
    private PrivateSchoolControllerImpl controller = new PrivateSchoolControllerImpl();
    private DaoCheckerImpl tested = new DaoCheckerImpl(printerMock, controller);

    @Test
    public void should_return_true_when_grade_book_is_empty() {
        Assertions.assertTrue(tested.checkIfGradeBookIsEmpty());
    }

    @Test
    public void should_return_false_when_grade_book_is_not_empty() {
        controller.addStudent("name");
        Assertions.assertFalse(tested.checkIfGradeBookIsEmpty());
    }

    @Test
    public void should_return_true_when_any_of_students_have_no_grades_for_any_subject() {
        controller.addStudent("name");
        Assertions.assertTrue(tested.checkIfNoneGradesForAnyStudentToSubject());
    }

    @Test
    public void should_return_false_when_all_students_have_at_elast_one_grade_for_all_subjects() {
        controller.addStudent("name");
        controller.addGrade(1, 1, 2);
        controller.addGrade(1, 2, 2);
        controller.addGrade(1, 3, 2);
        Assertions.assertFalse(tested.checkIfNoneGradesForAnyStudentToSubject());
    }

    @Test
    public void should_return_true_when_at_least_one_student_has_at_least_one_grade_1() {
        controller.addStudent("name");
        controller.addGrade(1, 1, 1);
        controller.addGrade(1, 2, 2);
        controller.addGrade(1, 3, 2);
        Assertions.assertTrue(tested.checkIfAnyoneHasGrade1());
    }

    @Test
    public void should_return_false_when_none_of_students_has_grade_1() {
        controller.addStudent("name");
        controller.addGrade(1, 1, 4);
        controller.addGrade(1, 2, 2);
        controller.addGrade(1, 3, 2);
        Assertions.assertFalse(tested.checkIfAnyoneHasGrade1());
    }


}