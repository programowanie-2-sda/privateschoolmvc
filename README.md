SZKOŁA

Jestes wlascicielem swojej wlasnej szkoly prywatnej.Do Twojej szkoly uczeszczaja najlepsi uczniowie w miescie
gdzie srednia ich ocena jest powyzej 4,3 (w systemie 1 - 6).Napisz program pozwalajacy wykonywac konkretne rzeczy
w szkole:

-zakladamy, że w szkole są tylko 3 przedmioty;

- W programie powinna byc opcja dodania osoby do dziennika

- Powinna byc opcja dodawania ocen konkretnemu uczniowi, do konkretnego przedmiotu (podczas dodawania oceny
powinien powstac nowy parametr "srednia dla przedmiotu", ktory bedzie modyfikowany podczas dodawania oraz
poprawiania ocen)

- Powinna byc opcja poprawiania ocen konkretnemu uczniowi (MAKSYMALNIE RAZ)
	-oceny moga byc poprawione na słabsze
	-jeżeli uczeń poprawi ocene na 1 to automatycznie wylatuje ze szkoły (to naprawde elitarna szkoła, nie ma miejsca
	na błedy)

- Powinna byc opcja zakonczenia roku szkolnego:
    -w ktorym sprawdzi sie obecny poziom sredniej kazdego ucznia. Jezeli jest on ponizej 4,3 powinien on wyleciec ze szkoly
    - automatyczne rozpoczecie nowego roku szkolnego po zaknczeniu straego, gdzie zostana uczniowie ktorzy nie
     wylecili na koniec roku i ich oceny zostana skasowane

- Dodatkowo opcja zakonczenia roku szkolnego powinna byc mozliwa tylko wtedy kiedy wszyscy uczniowie
poprawia ocene 1 na wyzsza.

-opcje wyswietlania:
	-wszytkich ocen danego ucznia i 1 przemiotu
	-wszystkich ocen danego ucznia i wszyskich przedmiotow
	-srednich dla danego ucznia dla wszytskich przedmiotów

Architektura projektu to MVC. W projekcie staramy sie uzywac lambd oraz strumieni. Do wszystkiego
piszemy testy jednostkowe.
