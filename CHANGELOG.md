## v.1.0
* program works with no bugs
* unit tests 
    - controller 97% coverage
    - view 56% coverage
* features:
   * Add new student
   * Add multiple grades for student and subject
   * Show all subjects and all grades for student
   * Show one subject and all grades for student
   * Show all subjects with averages for student
   * Revise grade for student and subject (one grade at once)
        * when student revise grade to 1 - he is being removed from a school
        * student can revise his grade just one time
   * Remove student from school
   * Finish school year
        * students with grade point average lower than 4,3 are being removed from a school
        * school year can be finish just when all students revise all their "1" grades 
        
 ## v.1.1
 * program works with no bugs
 * refactoring of controller done
 * removal of unit tests for controller (need to write new tests)
 * no new features
 
         
 ## v.1.2
 * program works with no bugs
 * adding Lombok package
 * refactoring of loop where iteration was used
 * no new features
 
  ## v.1.3
  * program works with no bugs
  * refactoring of view - adding new main menu and sub-menus
  * new features:
    * showing all students
    
   ## v.1.4 
  * program works with no bugs
  * refactoring of view:
    * refactoring UserOptionGetter into UserInput
    * add method getNextString
    * refactoring classes in view package in accordance to above changes
    
   ## v.1.5
   * program works with no bugs
   * refactoring of view:
        * using composite for all menus within program
        * removal of unnecessary methods from Printer class